import React from "react";
import { StyleSheet, Text, View,FlexAlignType } from "react-native";

const Flex = () => {
  return (
    <View style={{
      
      backgroundColor:"pink",
      flex:1,
      
    }}>
      <View style={{ flex:0.5,backgroundColor: "red",flexDirection:"row" }} >
        <View style={{ flex:0.5,backgroundColor:"black"}}></View>
        <View style={{ flex:0.5,backgroundColor:"white"}}></View>
        </View>
      <View style={{ flex:0.5, backgroundColor: "blue",flexDirection:"column" }} >
      <View style={{ flex:0.5,backgroundColor:"pink"}}></View>
        <View style={{ flex:0.5,backgroundColor:"orange"}}></View>
        <View style={{ flex:0.5,backgroundColor:"red"}}></View>
      </View>
      </View>
      
      
  );
};
export default Flex;