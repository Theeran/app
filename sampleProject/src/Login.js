import React,{useState} from "react";
import Icon from 'react-native-vector-icons/FontAwesome';
import {  Text, View,Image,TouchableOpacity, TextInput, ScrollView,Alert} from "react-native";
import auth from '@react-native-firebase/auth';
import Styles from './Components/Styles/Styles'
const login = ({navigation}) => {
 
  const [Mail,setMail] = React.useState('')
  const [Password,setPassword] = React.useState('')
  
  const validate = () => {
     if(Mail === ''){
      Alert.alert('Please fill the E-Mail Id')
    }
    else if(Password === ''){
      Alert.alert('Please fill the password')
    }
    else{
      auth()
      .signInWithEmailAndPassword (Mail,Password)
      .then(() => {
        navigation.navigate('Dashboard')
      })
      .catch(error => {
        if(error.code === 'auth/user-not-found'){
             Alert.alert('No user found');
        }
        if(error.code === 'auth/wrong-password'); {
          Alert.alert('Wrong password ');
        }
        console.error(error);
      });
    }
    
  }
    
    return (
      <ScrollView>
      <View style={Styles.l1}>
        <Text style={Styles.l2}>Welcome !</Text>
        <Text style={Styles.l3}>sign in to continue</Text>
        <TextInput style={Styles.l4} onChangeText={(text) => setMail(text)}
      placeholder="Enter your Email"
      value={Text}
></TextInput>
     <TextInput style={Styles.l5}secureTextEntry={true}onChangeText={(text) => setPassword(text)}
      placeholder="Enter your password "
      value={Text}
>
      </TextInput>
     <TouchableOpacity onPress={validate} style={Styles.l6}>
         <Text style={Styles.l7}>LOGIN</Text>
        </TouchableOpacity>
        <Text style={Styles.l8}>Forgot Password?</Text>
        <Text style={Styles.l9}>Social Media Login</Text>
        <View style={Styles.l10} >
        <Icon name="google" size={33} color="green" />
        <Icon name="facebook" size={33} color="blue" />
        <Icon name="apple" size={33} color="black" />
      </View>
      <View style={Styles.l11} >
      <Text style={Styles.l12}>Already have an account?<Text style={Styles.l13}>Sign in</Text></Text>
      </View>
      </View>
      </ScrollView>
);
};

  export default login;