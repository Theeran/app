import React from 'react';
import Icon from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Ionicons   from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons   from 'react-native-vector-icons/MaterialCommunityIcons'
import {useState, useEffect} from 'react';
import Styles from './Components/Styles/Styles'

import {Text, View, Image, TouchableOpacity,StyleSheet,FlatList, ScrollView} from 'react-native';

const App = () => {
  
    const [data, setData] = useState([])
    useEffect(() => {
      fetch('https://mocki.io/v1/6f92bd2d-089d-4638-9d85-8eac14ecdf09')
        .then(response => response.json())
        .then(asasd => setData(asasd))
        
        
  });
  return (
    <View style = {Styles.container1}>
      <View style={Styles.container2}>
          <View>
    <Text style={Styles.container4}> 
     <TouchableOpacity><Icon  name="sound-mix" size={12} color="#dbe8ff" />
</TouchableOpacity>            UDX 476893 <Text style={Styles.container5}>      $125</Text>       B.018      #101</Text>
</View>
<View style={Styles.container6}>
  <View style={Styles.container7}>
  <View style={Styles.container8}>
  <View style={Styles.container9}>
  </View>
  </View>
  </View>
</View>
<View style={Styles.container10}>
  <View >
<TouchableOpacity style={Styles.container11}><Icon   name="chevron-left" size={25} color="#b8c4fe" />
</TouchableOpacity>
</View>
<View style={Styles.container12}>
    <Text style={Styles.container13}>$</Text>
<Text style={Styles.container14}>124.99</Text>
<Text style={Styles.container15}>USD</Text>
<Text style={Styles.container16}>USh462933.80</Text>
</View>
<View>
 <TouchableOpacity ><Icon style={Styles.container17}  name="chevron-right" size={25} color="#b8c4fe" /></TouchableOpacity> 
  </View >
  <View style={Styles.container18}>
   <TouchableOpacity  style={Styles.container19}><Icon   name="circle-with-plus" size={50} color='#41aded'  /><Text style={Styles.container20}>Load</Text></TouchableOpacity> 

<TouchableOpacity style={Styles.container21}><Ionicons   name="sync-circle" size={50} color='#41aded'  /><Text style={Styles.container22}>Exchange</Text></TouchableOpacity> 
  </View>
  </View>
      </View>
      <View style={Styles.container3}>
          <View>
          <Text style={Styles.container23}>Transaction History </Text>
         <TouchableOpacity style={Styles.container24} ><Icon   name="chevron-right" size={20} color="lightblue" /></TouchableOpacity> 
          
         </View>
         <ScrollView>
         <View >
          <FlatList
         data={data}
        renderItem={({item}) => (
          <View style={Styles.container25}>
            <View style={{}}>
             <Image
            source={{uri:item.avatar}}
            style={Styles.container26}
          />
          <TouchableOpacity><Text style={Styles.container27}>
              {item.email}
            </Text></TouchableOpacity>
            <Text style={Styles.container28}>{item.id}</Text>
          </View>
          <View>
            <Text style={Styles.container29}>
              {item.first_name+item.last_name}
              </Text>
              </View>
            </View>
            
        )}
      />
       </View> 
      
       </ScrollView>
      </View>
      
<View style={Styles.container30}>
  <TouchableOpacity>
<FontAwesome5   name="wallet" size={23}  />
</TouchableOpacity>
<Text style={Styles.container31}>Balance</Text>
<TouchableOpacity>
<Icon style={Styles.container32}  name="credit-card" size={25}  />
</TouchableOpacity>
<Text style={Styles.container33}>Pay</Text>
<TouchableOpacity>
<MaterialCommunityIcons style={Styles.container34}  name="send-circle-outline" size={25} color={"black"} />
</TouchableOpacity>
<Text style={Styles.container35}>Send</Text>
<TouchableOpacity>
<Ionicons style={Styles.container36}  name="ios-repeat-sharp" size={25}  />
</TouchableOpacity>
<Text style={Styles.container37}>History</Text>
<TouchableOpacity>
<Icon style={Styles.container38}  name="dots-three-vertical" size={23}  />
</TouchableOpacity>
<Text style={Styles.container39}>More</Text>
</View>
    </View>
  );
};
// const style = StyleSheet.create({
//   container1:{
//     backgroundColor: 'white',
//     flex: 1, 
//     justifyContent: 'flex-start'
    
//   },
//  container2:{
     
//    flex:0.5,
//    backgroundColor:"#35438e"
//  },
//  container3:{
//   flex:0.5,
//   backgroundColor:"#ffffff"
// },
// container4:{
//   color:"#7180c0",
//   marginTop:20,
//   marginLeft:12
// },
// container5:{
//   color:"white"
// },
// container6:{
//   flexDirection:"row"
// },
// container7:{
//   height:0.4,
//   width:113,
//   backgroundColor:"grey",
//   marginLeft:60,
//   marginTop:5
// },
// container8:{
//   height:8,
//   width:8,backgroundColor:"white",
//   borderRadius:100,
//   marginLeft:113,
//   marginTop:-4
// },
// container9:{
//   height:0.4,
//   width:113,backgroundColor:"grey",
//   marginLeft:8,
//   marginTop:4
// },
// container10:{
// marginTop:25
// },
// container11:{
//   marginLeft:40
// },
// container12:{
//   marginLeft:60
// },
// container13:{
//   marginLeft:55,
//   marginTop:-20,fontSize:20,
//   color:"white"
// },
// container14:{
//   marginLeft:60,
//   marginTop:-18,
//   fontSize:40,
//   color:"white"
// },
// container15:{
//   marginLeft:165,
//   marginTop:-52,
//   color:"#909ee9"
// },
// container16:{
//   marginTop:30,
//   marginLeft:50,
//   fontSize:20,
//   color:"#909ee9"
// },
// container17:{
//   marginLeft:290,
//   marginTop:-85
// },
// container18:{
//   marginLeft:40
// },
// container19:{
//   marginLeft:50,
//   marginTop:59
// },
// container20:{
//   color:"#909ee9",
//   marginLeft:10
// },
// container21:{ 
//   marginLeft:190,
//   marginTop:-73
// },
// container22:{
//   color:"#909ee9"
// },
// container23:{
//   color:"black",
//   marginLeft:5
// },
// container24:{
//   marginLeft:335,
//   marginTop:-20
// },
// container25:{
//   marginTop:16
// },
// container26:{
//   height:55, 
//   width: 55,
//    borderRadius: 100
// },
// container27:{
//   marginLeft:90,
//   marginTop:-30
// },
// container28:{
//   marginLeft:340,
//   marginTop:-40,
//   fontSize:20,
//   color:"black"
// },
// container29:{
//   marginLeft:90,
//   color: 'black',
//    fontSize: 20
//    ,marginTop:-45
// },
// container30:{
//   flexDirection:"row",
//   marginLeft:30,
// },
// container31:{
//   marginTop:30,
//   marginLeft:-30
// },
// container32:{
//   marginLeft:22
// },
// container33:{
//   marginTop:29,
//   marginLeft:-20
// },
// container34:{
//   marginLeft:40
// },
// container35:{
//   marginTop:29,
//   marginLeft:-24
// },
// container36:{
//   marginLeft:35
// },
// container37:{
//   marginTop:29,
//   marginLeft:-26
// },
// container38:{
//   marginLeft:20
// },
// container39:{
//   marginTop:29,
//   marginLeft:-25
// },
// container40:{
 
// }
// });

export default App;


