import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  container1: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'flex-start',
  },
  container2: {
    flex: 0.5,
    backgroundColor: '#35438e',
  },
  container3: {
    flex: 0.5,
    backgroundColor: '#ffffff',
  },
  container4: {
    color: '#7180c0',
    marginTop: 20,
    marginLeft: 12,
  },
  container5: {
    color: 'white',
  },
  container6: {
    flexDirection: 'row',
  },
  container7: {
    height: 0.4,
    width: 113,
    backgroundColor: 'grey',
    marginLeft: 60,
    marginTop: 5,
  },
  container8: {
    height: 8,
    width: 8,
    backgroundColor: 'white',
    borderRadius: 100,
    marginLeft: 113,
    marginTop: -4,
  },
  container9: {
    height: 0.4,
    width: 113,
    backgroundColor: 'grey',
    marginLeft: 8,
    marginTop: 4,
  },
  container10: {
    marginTop: 25,
  },
  container11: {
    marginLeft: 40,
  },
  container12: {
    marginLeft: 60,
  },
  container13: {
    marginLeft: 55,
    marginTop: -20,
    fontSize: 20,
    color: 'white',
  },
  container14: {
    marginLeft: 60,
    marginTop: -18,
    fontSize: 40,
    color: 'white',
  },
  container15: {
    marginLeft: 165,
    marginTop: -52,
    color: '#909ee9',
  },
  container16: {
    marginTop: 30,
    marginLeft: 50,
    fontSize: 20,
    color: '#909ee9',
  },
  container17: {
    marginLeft: 290,
    marginTop: -85,
  },
  container18: {
    marginLeft: 40,
  },
  container19: {
    marginLeft: 50,
    marginTop: 59,
  },
  container20: {
    color: '#909ee9',
    marginLeft: 10,
  },
  container21: {
    marginLeft: 190,
    marginTop: -73,
  },
  container22: {
    color: '#909ee9',
  },
  container23: {
    color: 'black',
    marginLeft: 5,
  },
  container24: {
    marginLeft: 335,
    marginTop: -20,
  },
  container25: {
    marginTop: 16,
  },
  container26: {
    height: 55,
    width: 55,
    borderRadius: 100,
  },
  container27: {
    marginLeft: 90,
    marginTop: -30,
  },
  container28: {
    marginLeft: 340,
    marginTop: -40,
    fontSize: 20,
    color: 'black',
  },
  container29: {
    marginLeft: 90,
    color: 'black',
    fontSize: 20,
    marginTop: -45,
  },
  container30: {
    flexDirection: 'row',
    marginLeft: 30,
  },
  container31: {
    marginTop: 30,
    marginLeft: -30,
  },
  container32: {
    marginLeft: 22,
  },
  container33: {
    marginTop: 29,
    marginLeft: -20,
  },
  container34: {
    marginLeft: 40,
  },
  container35: {
    marginTop: 29,
    marginLeft: -24,
  },
  container36: {
    marginLeft: 35,
  },
  container37: {
    marginTop: 29,
    marginLeft: -26,
  },
  container38: {
    marginLeft: 20,
  },
  container39: {
    marginTop: 29,
    marginLeft: -25,
  },
  w1: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'flex-start',
  },
  w2: {
    height: 400,
    width: 500,
  },
  w3: {
    fontWeight: 'bold',
    fontSize: 40,
    textAlign: 'center',
    marginTop: 20,
    color: 'black',
  },
  w4: {
    textAlign: 'center',
    color: 'black',
  },
  w5: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
  },
  w6: {
    backgroundColor: 'blue',
    height: 27,
    width: 200,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'darkblue',
  },
  w7: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  w8: {
    textAlign: 'center',
    color: 'white',
    marginTop: 3,
  },
  w9: {
    backgroundColor: 'white',
    height: 27,
    width: 200,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'darkblue',
  },
  w10: {
    textAlign: 'center',
    color: 'blue',
    marginTop: 3,
  },
  w11: {
    backgroundColor: 'blue',
    height: 27,
    width: 200,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'darkblue',
    marginTop: 15,
  },
  w12: {
    textAlign: 'center',
    color: 'white',
    marginTop: 3,
  },
  l1: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 70,
    marginLeft: 20,
  },
  l2: {
    color: '#0e48ab',
    fontSize: 30,
    fontWeight: '500',
  },
  l3: {fontSize: 15, color: 'black'},
  l4: {
    height: 36,
    width: 300,
    borderBottomWidth: 1,
    marginTop: 100,
    marginLeft: 5,
  },
  l5: {
    height: 36,
    width: 300,
    borderBottomWidth: 1,
    marginTop: 50,
    marginLeft: 5,
  },
  l6: {
    backgroundColor: 'blue',
    height: 27,
    width: 200,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'darkblue',
    marginLeft: 50,
    marginTop: 60,
  },
  l7: {
    textAlign: 'center',
    color: 'white',
    marginTop: 3,
  },
  l8: {marginLeft: 100, marginTop: 10},
  l9: {
    marginLeft: 80,
    marginTop: 10,
    color: 'blue',
    fontSize: 19,
    marginTop: 60,
  },
  l10: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 30,
  },
  l11: {
    flex: 0.5,
    marginTop: 30,
    marginLeft: 60,
  },
  l12: {
    color: 'black',
    fontSize: 13,
  },
  l13: {
    color: 'blue',
    fontSize: 15,
  },
  s1: {
    marginLeft: 80,
    color: 'blue',
    fontSize: 19,
    marginTop: 40,
  },
  d1: {
    height: 150,
    width: '100%',
    backgroundColor: 'pink',
    marginTop: 10,
  },
  d2: {
    alignItems: 'center',
  },
  d3: {
    fontSize: 28,
  },
  d4: {
    fontSize: 25,
  },
  d5: {
    marginHorizontal: 20,
    margin: -30,
  },
  d6: {
    height: 80,
    width: 80,
    borderRadius: 10,
  },
  d7: {
    flex: 1,
  },
});
