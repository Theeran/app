import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import Styles from './Components/Styles/Styles'
const Welcomepage = ({navigation}) => {
  return (
    <View
      style={Styles.w1}>
      <Image
        source={require('../assets/man.png')}
        style={Styles.w2}
      />
      <Text
        style={Styles.w3}>
        Hello !
      </Text>
      <Text style={Styles.w4}>
        Best place to write life stories and
      </Text>
      <Text style={Styles.w4}>
        {' '}
        share your journey experiences.
      </Text>

      <View
        style={Styles.w5}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Login')}
          style={Styles.w6}>
          <Text style={Styles.w8}>
            Login
          </Text>
        </TouchableOpacity>
        <View
          style={Styles.w7}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Signup')}
            style={Styles.w9}>
            <Text style={Styles.w10}>
              SIGNUP
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate('Theeranassignment1')}
            style={Styles.w11}>
            <Text style={Styles.w12}>
              Assignment1
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Welcomepage;
