import React from 'react';
import Styles from './Components/Styles/Styles'
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Image,
  Text,
  StatusBar,
} from 'react-native';
const DATA = [
  {
    discount: '10,000',
    endsin: 'Today',
    name: 'Mobile',
    rate: '12,000',
    url: 'https://m.media-amazon.com/images/I/41apNNfINPL.__AC_SY200_.jpg',
  },
  {
    discount: '150',
    endsin: 'Today',
    name: 'Watch',
    rate: '214',
    url: 'https://m.media-amazon.com/images/I/41TvSQNosiL.__AC_SY200_.jpg',
  },
  {
    discount: '250',
    endsin: 'Today',
    name: 'HandBag',
    rate: '319',
    url: 'https://m.media-amazon.com/images/I/41x7KLp5JiL.__AC_SY200_.jpg',
  },
  {
    discount: '1000',
    endsin: 'Today',
    name: 'Dress',
    rate: '2000',
    url: 'https://m.media-amazon.com/images/I/51eibVsPieL.__AC_SY200_.jpg',
  },
  {
    discount: '800',
    endsin: 'Today',
    name: 'Headset',
    rate: '1500',
    url: 'https://m.media-amazon.com/images/I/41LodhV-waL.__AC_SY200_.jpg',
  },
  {
    discount: '750',
    endsin: 'Today',
    name: 'Pant',
    rate: '1200',
    url: 'https://m.media-amazon.com/images/I/41QFLArTr+L.__AC_SY200_.jpg',
  },
];
const App = () => {
  const Theeran = ({item}) => {
    return (
      <View 
        style={Styles.d1}>
          
        <View style={Styles.d2}>
          <Text style={Styles.d3}>{item.name} </Text>
          <Text style={Styles.d4}>{item.discount}</Text>
        </View>
        <View style={Styles.d5}>
          <Image
            source={{uri: item.url}}
            style={Styles.d6}
          />
        </View>
      </View>
    );
  };

 
    return (
     <View style={Styles.d7}>
       <FlatList data={DATA} renderItem={Theeran} />
     </View>
  );
};
export default App;