import React from "react";
import {  Text, View,Image,TouchableOpacity,} from "react-native";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Page2 from "./Page2";
import Page1 from "./Page1";

const Tab = createMaterialBottomTabNavigator();
const Page0= ({navigation}) => {
  
    return (
        
            
              
      <View style={{ backgroundColor:"white",flex:1,justifyContent:'flex-start',}}>
          <Text style={{fontWeight:'bold',fontSize:40,textAlign:'center',marginTop:20,color:"black"}}>Hello !</Text>
          <Text style={{textAlign:'center',color:"black"}}>Best place to write life stories and</Text> 
          <Text style={{textAlign:'center',color:"black"}}> share your journey experiences.</Text>
          
      <View style={{alignItems:'center',justifyContent:'center',marginTop:40}}>
      <TouchableOpacity onPress={()=>navigation.navigate('Page1')} style={{backgroundColor:"blue",height:27,width:200,borderRadius:5,borderWidth:1,borderColor:"darkblue"}}>
         
        <Text style={{textAlign:"center",color:"white",marginTop:3}}>Login</Text>
        </TouchableOpacity>
        <View style={{alignItems:'center',justifyContent:'center',marginTop:20}}>
      <TouchableOpacity onPress={()=>navigation.navigate('Page2')} style={{backgroundColor:"white",height:27,width:200,borderRadius:5,borderWidth:1,borderColor:"darkblue"}}>
         
        <Text style={{textAlign:"center",color:"blue",marginTop:3}}>SIGNUP</Text>
        </TouchableOpacity>
        </View>
        </View>
        <Tab.Navigator>
          
       <Tab.Screen  name="Page1" component={Page1} />
       <Tab.Screen name="Page2" component={Page2} />
     </Tab.Navigator>
          </View>
      
        
    );
};

  export default Page0;