import React,{useState} from "react";
import Icon from 'react-native-vector-icons/FontAwesome';
import {  Text, View,Image,TouchableOpacity, TextInput, ScrollView,Alert} from "react-native";
import auth from '@react-native-firebase/auth';
import Styles from './Components/Styles/Styles'
const signup = ({navigation}) => {
  const [Name,setName] = React.useState('')
  const [Mail,setMail] = React.useState('')
  const [Password,setPass] = React.useState('')
  
  const validate = () => {
    if(Name === ''){
      Alert.alert('Please fill the Username')
    }
    else if(Mail ===''){
      Alert.alert('Please fill the E-Mail Id')
    }
    else if(Password === ''){
      Alert.alert('Please fill the password')
    }
    else{
      auth()
      .createUserWithEmailAndPassword (Mail,Password)
       .then(() => {
        navigation.navigate('Login');
      })
      .catch(error => {
        if(error.code === 'auth/email-already-in-use') {
           Alert.alert('That email address is already in use!');
        }
        if (error.code === 'auth/invalid-email') {
          Alert.alert('That email address is invalid!');
        }
        console.error(error);
      });
    }
    
  }
    return (
      <ScrollView>
      <View style={Styles.l1}>
        <Text style={Styles.l2}>Hi!</Text>
        <Text style={Styles.l3}>Create a new account</Text>
        <TextInput style={Styles.l5}onChangeText={(text) => setName(text)}
      placeholder="Enter your Name"value={Text}
></TextInput>
        <TextInput style={Styles.l5}onChangeText={(text) => setMail(text)}
      placeholder="Enter your Email "
      value={Text}
></TextInput>
     <TextInput style={Styles.l5} onChangeText={(text) => setPass(text)}
      secureTextEntry={true}
      placeholder="Enter your password "
      value={Text}
></TextInput>
     <TouchableOpacity onPress= {validate} style={Styles.l6}>
         <Text style={Styles.l7}>SIGN UP</Text>
        </TouchableOpacity>
        <Text style={Styles.l8}>Forgot Password?</Text>
        
        
        <Text style={Styles.s1}>Social Media Login</Text>
     
        <View style={Styles.l10} >
        
        <Icon name="google" size={33} color="green" />
        <Icon name="facebook" size={33} color="blue" />
        <Icon name="apple" size={33} color="black" />
      </View>
      <View style={Styles.l11} >
      <Text style={Styles.l12}>Already have an account?<Text style={Styles.l13}>Sign in</Text></Text>
      </View>
      </View>
      </ScrollView>

);
};

  export default signup;