import React, { useEffect } from "react";
import { View, Image } from "react-native";
const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('Welcomepage')
        }, 1000);
    }, [])
    return (
        <View>
            <Image style={{ height:700,width:370 }} source={require('../assets/shop.jpg')} ></Image>
        </View>
    );
};
export default Splash;