import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

 import Splash from '../Splash'
//  import Page0 from '../Page0'
  import Welcomepage from '../Welcomepage'
//  import Page2 from '../Page2'
//  import Page1 from '../Page1'
 import Login from '../Login'
 import Signup from '../Signup'
  import Dashboard from '../Dashboard'
  import Dashboard2 from '../Dashboard2'
  import Theeranassignment1 from '../Theeranassignment1'
  
const RootStack = createNativeStackNavigator();
const AppRouter = () => {
    return (
        <NavigationContainer>
            
   
             <RootStack.Navigator>
                <RootStack.Screen name="Splash" component={Splash}/>
                {/* <RootStack.Screen name="Page0" component={Page0} /> */}
                 <RootStack.Screen name="Welcomepage" component={Welcomepage} /> 

                {/* <RootStack.Screen  name="Page2" component={Page2}/>
                <RootStack.Screen name="Page1" component={Page1}/> */}


                <RootStack.Screen name="Login" component={Login} />
                <RootStack.Screen name="Signup" component={Signup} />
                 <RootStack.Screen name="Dashboard" component={Dashboard} />
                 <RootStack.Screen name="Dashboard2" component={Dashboard2} /> 
                 <RootStack.Screen name="Theeranassignment1" component={Theeranassignment1} />
               
            </RootStack.Navigator> 
        </NavigationContainer>
    )
}
export default AppRouter;