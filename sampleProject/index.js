/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Approuter from './src/Router/Approuter'

AppRegistry.registerComponent(appName, () => Approuter)
